package com.oramon.stringcalculator.impl;

import com.oramon.stringcalculator.interfaces.ListProcessor;

import java.util.List;

/**
 * Created by vasilis on 3-7-17.
 */
public class ListProcessorImpl implements ListProcessor {

    @Override
    public int sumAllTheNumber(String[] stringNumbers) {
        Integer result = 0;
        Integer[] numbers = new Integer[stringNumbers.length];
        for (int i = 0; i < stringNumbers.length; i++) {
            numbers[i] = Integer.parseInt(stringNumbers[i]);
        }



        for (Integer p : numbers) {
            result += p;
        }
        return result;
    }
}


